package com.tm.task03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Task03Application {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Task03Application.class, args);

		String url = "https://www.opennet.ru/opennews/mini.shtml";
		String url2 = "https://mirknig.su";
		String dataPars = "<td class=tdate>04.09.2021";
		String dataPars2 = "Сегодня";


		Parses parses = context.getBean(Parses.class);
		Counters counters = context.getBean(Counters.class);

		CountResult countResult = (CountResult) context.getBean(CountResult.class);
		countResult.getParses().setUrl(url);
		countResult.getCounters().setDataPars(dataPars);
		countResult.getCounters().setStrPars(countResult.getParses().parsePage());

		CountResult countResult1 = (CountResult) context.getBean(CountResult.class);
		parses.setUrl(url2);
		countResult1.getCounters().setDataPars(dataPars2);
		countResult1.getCounters().setStrPars(parses.parsePage());


		System.out.println(countResult.getCounters().countNews());
		System.out.println(countResult1.getCounters().countNews());
	}
}
