package com.tm.task03;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Scope("prototype")
public class PageParseImpl implements Parses{
    private String url;

    @Override
    public String parsePage(){
        String contentPage="";
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            final HttpGet httpGet = new HttpGet(this.url);
            httpGet.setHeader("Accept", "application/xml");
            httpGet.setHeader("User-Agent",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) " +
                            "Chrome/23.0.1271.95 Safari/537.11");

            try (final CloseableHttpResponse response = httpclient.execute(httpGet)) {
                contentPage = EntityUtils.toString(response.getEntity());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contentPage;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }
}
