package com.tm.task03;

public interface Parses {
    public String parsePage();
    public void setUrl(String url);
}
