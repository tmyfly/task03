package com.tm.task03;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Scope("prototype")
public class NewsCountImpl implements Counters {
    private String strPars;
    private String dataPars;

    @Override
    public int countNews(){
        int count = 0;

        Pattern pattern = Pattern.compile(this.dataPars);
        Matcher matcher = pattern.matcher(this.strPars);
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    @Override
    public void setStrPars(String strPars) {
        this.strPars = strPars;
    }

    @Override
    public void setDataPars(String dataPars) {
        this.dataPars = dataPars;
    }
}
