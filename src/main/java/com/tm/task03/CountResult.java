package com.tm.task03;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class CountResult {
    @Autowired
    private Counters counters;
    @Autowired
    private Parses parses;

    public Counters getCounters() {
        return counters;
    }

    public Parses getParses() {
        return parses;
    }
}
